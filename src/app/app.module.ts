import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HeroComponent } from './hero/hero.component';
import { BlogShowcaseComponent } from './blog-showcase/blog-showcase.component';
import { BlogFooterComponent } from './blog-footer/blog-footer.component';
import { BlogPostComponent } from './blog-showcase/blog-post/blog-post.component';
import { PromoComponent } from './blog-showcase/promo/promo.component';
import { FeatureComponent } from './blog-showcase/feature/feature.component';
import { BlogPageComponent } from './blog-page/blog-page.component';
import { ExcerptComponent } from './excerpt/excerpt.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HeroComponent,
    BlogShowcaseComponent,
    BlogFooterComponent,
    BlogPostComponent,
    PromoComponent,
    FeatureComponent,
    BlogPageComponent,
    ExcerptComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
