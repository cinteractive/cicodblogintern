import { Router } from '@angular/router';
import { Email } from './../models/emails.model';
import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  constructor(
    private api: ApiService,
    private route: Router
  ) { }

  resp = this.api.getApi();


  ngOnInit(): void {

  }

  postIt() {
    const userEmail: Email = {
      email : (document.getElementById('emailbox') as HTMLInputElement).value
    };
    if (userEmail.email === '') {
      alert('Please enter your email address');
      return null;
    }
    else {
      this.api.postEmail(userEmail).subscribe(
        response => console.log(response),
        error => console.log(error)
      );
      alert('You have subscribed successfully');
      return userEmail.email = '';
    }
  }






}
