import { Blog } from './../models/blog.model';
import { ApiService } from './../api.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-excerpt',
  templateUrl: './excerpt.component.html',
  styleUrls: ['./excerpt.component.scss']
})
export class ExcerptComponent implements OnInit, OnDestroy {

 Blog: Blog;
 subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit(): void {
      let index;
      this.route.params.subscribe(
         (params) => {
           index = params.id;
         }
       );
      const resp = this.api.getApi();

      this.subscription = resp.subscribe((data => {
        this.Blog = data[index];
      }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
