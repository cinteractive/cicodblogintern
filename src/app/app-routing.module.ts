import { ExcerptComponent } from './excerpt/excerpt.component';
import { BlogShowcaseComponent } from './blog-showcase/blog-showcase.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '',  component: BlogShowcaseComponent},
  { path: ':id', component: ExcerptComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
