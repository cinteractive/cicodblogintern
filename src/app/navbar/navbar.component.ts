import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor() { }
  ngOnInit(): void {
  }

  onHamburger() {
    let x = document.getElementById('navMenu');
    if (x.className === 'nav') {
      x.className += ' responsive';
    }
    else {
      x.className = 'nav';
    }
      }


}
