import { Component } from '@angular/core';
import { Blog } from './models/blog.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cidod-blog';

}
