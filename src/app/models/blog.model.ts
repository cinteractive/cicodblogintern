export class Blog {
    public id: number;
   public blogName: string;
   public blogImg: string;
   public blogImg2: string;
   public blogImg3: string;
   public blogDescription: string;
   public blogContent: string;
   public authorName: string;
   public authorPic: string;

    // tslint:disable-next-line: max-line-length
    constructor(id: number, name: string, imageUrl: string, imageUrl2: string, imageUrl3: string, Desc: string, content: string, authorName: string, authorPic: string) {
        this.authorName = authorName;
        this.authorPic = authorPic;
        this.id = id;
        this.blogName = name;
        this.blogImg = imageUrl;
        this.blogImg2 = imageUrl2;
        this.blogImg3 = imageUrl3;
        this.blogDescription = Desc;
        this.blogContent = content;
    }

}

