import { Email } from './models/emails.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient) { }

  getApi(): Observable<any> {
    return this.http.get(environment.apiUrl[0]);
  }

  postEmail(email: Email): Observable<any> {
    return this.http.post(environment.apiUrl[1], email);
  }
}
