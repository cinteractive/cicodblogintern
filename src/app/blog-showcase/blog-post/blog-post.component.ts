import { Blog } from './../../models/blog.model';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.scss']
})
export class BlogPostComponent implements OnInit {
  @Input() Blogs: Blog;
  @Output() id = new EventEmitter<number>();
  isTrue = 1;

  onBlogChosen() {
    this.id.emit(this.isTrue);
    console.log('was clicked!')
  }



  constructor() { }

  ngOnInit(): void {
  }

}
