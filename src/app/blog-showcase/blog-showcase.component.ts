import { ApiService } from './../api.service';
// import { environment } from './../../environments/environment';
// import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Identifiers } from '@angular/compiler';

@Component({
  selector: 'app-blog-showcase',
  templateUrl: './blog-showcase.component.html',
  styleUrls: ['./blog-showcase.component.scss']

})
export class BlogShowcaseComponent implements OnInit, OnDestroy{
   Blogs = [];




    obj;
  constructor(private api: ApiService){ }

  public resp = this.api.getApi();

  ngOnInit() {
    this.resp.subscribe((data => {
      const index = data.length;
      for (let i = 0; i <= index - 1 ; i++) {
        this.Blogs.push(data[i]);
      }
      console.log(index);
    }) );
  }

  getIdnumber(data: number) {
    console.log(data);
  }

  ngOnDestroy() {

  }

}


