import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrls: ['./blog-page.component.scss']
})
export class BlogPageComponent implements OnInit {
  blogData = [];
  constructor(public apiService: ApiService) { }

  ngOnInit(): void {
    const resp = this.apiService.getApi();
    resp.subscribe((data) => {
     for (let i = 0; i <= 7; i++) {
       this.blogData.push(data[i]);
     }
     console.log(this.blogData);
    })
    }
  }


